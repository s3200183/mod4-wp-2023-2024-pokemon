package nl.utwente.mod4.pokemon.dao;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import nl.utwente.mod4.pokemon.Utils;
import nl.utwente.mod4.pokemon.model.Trainer;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;

public enum TrainerDao {

    INSTANCE;

    private static final String ORIGINAL_TRAINERS = Utils.getAbsolutePathToResources() + "/default-trainers-dataset.json";
    private static final String TRAINERS = Utils.getAbsolutePathToResources() + "/trainers.json";

    private HashMap<String, Trainer> trainers = new HashMap<>();

    public void delete(String id) {
        if(trainers.containsKey(id)) {
            trainers.remove(id);
        } else {
            throw new NotFoundException("Trainer '" + id + "' not found.");
        }
    }

    public List<Trainer> getTrainers(int pageSize, int pageNumber) {
        var list = new ArrayList<>(trainers.values());
        return (List<Trainer>) Utils.pageSlice(list,pageSize,pageNumber);
    }

    public Trainer getTrainer(String id) {
        var pt = trainers.get(id);

        if (pt == null) {
            throw new NotFoundException("Pokemon '" + id + "' not found!");
        }

        return pt;
    }

    public void load() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File source = existsTrainers() ?
                new File(TRAINERS) :
                new File(ORIGINAL_TRAINERS);
        Trainer[] arr = mapper.readValue(source, Trainer[].class);

        Arrays.stream(arr).forEach(trainer -> trainers.put(trainer.id, trainer));
    }

    public void save() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        File destination = new File(TRAINERS);

        writer.writeValue(destination, trainers.values());
    }

    private boolean existsTrainers() {
        File f = new File(TRAINERS);
        return f.exists() && !f.isDirectory();
    }

    private int getMaxId() {
        Set<String> ids = trainers.keySet();
        return ids.isEmpty() ? 0 : ids.stream()
                .map(Integer::parseInt)
                .max(Integer::compareTo)
                .get();
    }


    public Trainer create(Trainer newTrainer) {
        String nextId = "" + (getMaxId() + 1);

        newTrainer.id = nextId;
        newTrainer.created = Instant.now().toString();
        newTrainer.lastUpDate = Instant.now().toString();
        trainers.put(nextId,newTrainer);

        return newTrainer;
    }

    public Trainer update(Trainer updated) {
        if(!updated.isValid())
            throw new BadRequestException("Invalid trainer.");
        if(trainers.get(updated.id) == null)
            throw new NotFoundException("Trainer id '" + updated.id + "' not found.");

        updated.lastUpDate = Instant.now().toString();
        trainers.put(updated.id,updated);

        return updated;
    }

    public int getTotalTrainers() {
        return trainers.keySet().size();
    }
}
